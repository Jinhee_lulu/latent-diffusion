import argparse, os, sys, glob

import cv2
import torch.nn.functional as F
from PIL._imaging import display
from omegaconf import OmegaConf
from PIL import Image
from torchvision import transforms
from tqdm import tqdm
import numpy as np
import torch

from ldm.models.diffusion.ddpm import DDPM
from ldm.modules.encoders.modules import PoreEncoder
from main import instantiate_from_config
from ldm.models.diffusion.ddim import DDIMSampler






def make_batch(image, mask, device):
    image = np.array(Image.open(image).convert("RGB"))
    image = image.astype(np.float32)/255.0
    image = image[None].transpose(0,3,1,2)
    image = torch.from_numpy(image)

    mask = np.array(Image.open(mask).convert("L"))
    mask = mask.astype(np.float32)/255.0
    mask = mask[None,None]
    mask[mask < 0.5] = 0
    mask[mask >= 0.5] = 1
    mask = torch.from_numpy(mask)

    masked_image = (1-mask)*image

    batch = {"image": image, "mask": mask, "masked_image": masked_image}
    for k in batch:
        batch[k] = batch[k].to(device=device)
        batch[k] = batch[k]*2.0-1.0
    return batch


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--indir",
        type=str,
        nargs="?",
        help="dir containing image-mask pairs (`example.png` and `example_mask.png`)",
    )
    parser.add_argument(
        "--outdir",
        type=str,
        nargs="?",
        help="dir to write results to",
    )
    parser.add_argument(
        "--steps",
        type=int,
        default=50,
        help="number of ddim sampling steps",
    )
    opt = parser.parse_args()

    masks = sorted(glob.glob(os.path.join(opt.indir, "*_mask.png")))
    images = [x.replace("_mask.png", ".png") for x in masks]
    print(f"Found {len(masks)} inputs.")


    # 256
    # config = OmegaConf.load("../models/ldm/mycode/config256.yaml")
    # model = instantiate_from_config(config.model)
    # model.load_state_dict(torch.load("../models/ldm/mycode/256_000993.ckpt", map_location='cuda:0')["state_dict"], strict=False)

    # 512
    config = OmegaConf.load("../models/ldm/mycode/2023-05-25T02-17-00-project.yaml")
    model = instantiate_from_config(config.model)
    model.load_state_dict(torch.load("../models/ldm/mycode/epoch=000056.ckpt", map_location='cuda:0')["state_dict"], strict=False)

    ref = Image.open("C:/Users/kator/PycharmProjects/latent-diffusion/data/ref/A_forward.jpg").convert("RGB")
    # Define the image transformation
    preprocess = transforms.Compose([
        transforms.Resize((512, 512)),  # Resize the image to match the expected input size
        transforms.ToTensor(),  # Convert the image to a tensor
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),  # Normalize the image
    ])
    ref_image = preprocess(ref).unsqueeze(0)  # Add a batch dimensio

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model = model.to(device)
    sampler = DDIMSampler(model)

    print('CUDA:' , torch.cuda.is_available())  #__is_unconditional__

    os.makedirs(opt.outdir, exist_ok=True)
    with torch.no_grad():  # inference 시 gradient update 계산을 하지 않음
        with model.ema_scope():  # Expotential moving average(EMA): gradient 튀는 것 방지 -> overfitting 방지, generalization 향상
            for image, mask in tqdm(zip(images, masks)):
                outpath = os.path.join(opt.outdir, os.path.split(image)[1])
                batch = make_batch(image, mask, device=device)  # image, mask, masked image 묶음 생성

                # encode masked image and concat downsampled mask
                # 원본 이미지를 VQ autoencoder의 Encoder로 latent Diffusion의 input image로 만듦. 256x256 -> 64x64 (config.yaml로 조절가능)
                #c1 = model.cond_stage_model.encode(batch["image"])  # encoder
                #c = model.cond_stage_model.encode(batch["masked_image"])  # encoder  원래 inpaint는 mask를 입혀서 mask 영역만 생성하는 거지만, 내가 하려고 하는건 단순히 모공 map을 입히는 거임 -> 그래서 주석처리
                #cc = torch.nn.functional.interpolate(batch["mask"], size=c.shape[-2:]) # 마찬가지 이유로 주석처리
                # c = torch.cat((c1, cc), dim=1)

                #c = model.get_learned_conditioning(opt.n_samples * [prompt])   # text2img에서는 prompt를 encoding해서 condition으로 사용함
                #c = model.cond_stage_mode.encode(bacth["모공map"])  # 이렇게 해서 넣어야함

                # # encode masked image and concat downsampled mask
                # input_channels = 3  # RGB images
                # embed_dim = 512  # Dimension of the image embeddings
                # encoder = PoreEncoder(input_channels, embed_dim)
                # c1 = encoder.encode(ref_image)


                c = model.cond_stage_model.encode(batch["masked_image"])
                cc = torch.nn.functional.interpolate(batch["mask"],  size=c.shape[-2:])
                c = torch.cat((c, cc), dim=1)

                shape = (c.shape[1] - 1,) + c.shape[2:]

                #ldm
                # = sample encoder + p_sample_loop

                samples_ddim, _ = sampler.sample(S=opt.steps,
                                                 conditioning=c,
                                                 batch_size=c.shape[0],
                                                 shape=shape,
                                                 verbose=False)

                # = sample vae.decode(img)
                x_samples_ddim = model.decode_first_stage(samples_ddim)   # decoder

                image = torch.clamp((batch["image"]+1.0)/2.0, min=0.0, max=1.0)   # 원본 이미지
                mask = torch.clamp((batch["mask"]+1.0)/2.0, min=0.0, max=1.0)     # binary인 mask
                masked = torch.clamp((batch["masked_image"] + 1.0) / 2.0, min=0.0, max=1.0)  # binary인 mask
                predicted_image = torch.clamp((x_samples_ddim+1.0)/2.0,   min=0.0, max=1.0)   # masking 영역에 넣기 위해 임의로 만들어낸 이미지
                #inpainted = (1 - mask) * image + mask * predicted_image  # 원본이미지에 합성한 것


                outpath=outpath.split(".png")[0]
                ori_outpath = outpath + ".png"
                mask_outpath = outpath + "_mask.png"
                masked_outpath = outpath + "_masked.png"
                inpainted_outpath = outpath + "_inpainted.png"


                ori_img = image.cpu().numpy().transpose(0, 2, 3, 1)[0] * 255
                mask_img = mask.cpu().numpy().transpose(0, 2, 3, 1)[0] * 255
                masked_img = masked.cpu().numpy().transpose(0, 2, 3, 1)[0] * 255
                inpainted_img = predicted_image.cpu().numpy().transpose(0, 2, 3, 1)[0] * 255

                Image.fromarray(ori_img.astype(np.uint8)).save(ori_outpath)
                mask_img= np.squeeze(mask_img, axis=2)
                Image.fromarray(mask_img.astype(np.uint8)).save(mask_outpath)
                Image.fromarray(masked_img.astype(np.uint8)).save(masked_outpath)
                Image.fromarray(inpainted_img.astype(np.uint8)).save(inpainted_outpath)
