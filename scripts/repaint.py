import argparse, os, sys, glob

import cv2
import torch.nn.functional as F
from PIL._imaging import display
from omegaconf import OmegaConf
from PIL import Image
from torchvision import transforms
from tqdm import tqdm
import numpy as np
import torch

from ldm.models.diffusion.ddpm import DDPM
from main import instantiate_from_config
from ldm.models.diffusion.ddim import DDIMSampler


def make_batch(image, mask, device):
    image = np.array(Image.open(image).convert("RGB"))
    image = image.astype(np.float32)/255.0
    image = image[None].transpose(0,3,1,2)
    image = torch.from_numpy(image)

    mask = np.array(Image.open(mask).convert("L"))
    mask = mask.astype(np.float32)/255.0
    mask = mask[None,None]
    mask[mask < 0.5] = 0
    mask[mask >= 0.5] = 1
    mask = torch.from_numpy(mask)

    masked_image = (1-mask)*image

    batch = {"image": image, "mask": mask, "masked_image": masked_image}
    for k in batch:
        batch[k] = batch[k].to(device=device)
        batch[k] = batch[k]*2.0-1.0
    return batch


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--indir",
        type=str,
        nargs="?",
        help="dir containing image-mask pairs (`example.png` and `example_mask.png`)",
    )
    parser.add_argument(
        "--outdir",
        type=str,
        nargs="?",
        help="dir to write results to",
    )
    parser.add_argument(
        "--steps",
        type=int,
        default=50,
        help="number of ddim sampling steps",
    )
    opt = parser.parse_args()

    masks = sorted(glob.glob(os.path.join(opt.indir, "*_mask.png")))
    images = [x.replace("_mask.png", ".png") for x in masks]
    print(f"Found {len(masks)} inputs.")

    #256
    #config = OmegaConf.load("../models/ldm/ffhq256_2/config.yaml")
    #model = instantiate_from_config(config.model)
    #model.load_state_dict(torch.load("../models/ldm/mycode/256_000993.ckpt", map_location='cuda:0')["state_dict"], strict=False)

    # 512
    config = OmegaConf.load("../models/ldm/mycode/test.yaml")
    model = instantiate_from_config(config.model)
    model.load_state_dict(torch.load("../models/ldm/mycode/512_000458.ckpt", map_location='cuda:0')["state_dict"], strict=False)

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model = model.to(device)
    sampler = DDIMSampler(model)

    print('CUDA:' , torch.cuda.is_available())  #__is_unconditional__

    os.makedirs(opt.outdir, exist_ok=True)
    with torch.no_grad():
        with model.ema_scope():
            for image, mask in tqdm(zip(images, masks)):
                outpath = os.path.join(opt.outdir, os.path.split(image)[1])
                batch = make_batch(image, mask, device=device)

                # encode masked image and concat downsampled mask
                c1 = model.cond_stage_model.encode(batch["image"])  # encoder
                c = model.cond_stage_model.encode(batch["masked_image"])  # encoder
                cc = torch.nn.functional.interpolate(batch["mask"], size=c.shape[-2:])

                # check_img = (c1.cpu().numpy()[0])
                # img_norm = cv2.normalize(check_img, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
                # img = cv2.convertScaleAbs(img_norm, alpha=(255.0 / np.amax(img_norm)))
                # img = img.transpose((1, 2, 0))
                # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                # img = cv2.resize(img, (256, 256))
                # cv2.imshow("c1", img)
                #
                # check_img = (c.cpu().numpy()[0])
                # img_norm = cv2.normalize(check_img, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
                # img = cv2.convertScaleAbs(img_norm, alpha=(255.0 / np.amax(img_norm)))
                # img = img.transpose((1, 2, 0))
                # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                # img = cv2.resize(img, (256, 256))
                # cv2.imshow("c", img)

                c = torch.cat((c, cc), dim=1)
                shape = (c.shape[1] - 1,) + c.shape[2:]

                #ldm
                # = sample encoder + p_sample_loop
                samples_ddim, _ = sampler.sample(S=opt.steps,
                                                 conditioning=c,
                                                 batch_size=c.shape[0],
                                                 shape=shape,
                                                 verbose=False)

                # check_img=(samples_ddim.cpu().numpy()[0])
                # img_norm = cv2.normalize(check_img, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
                # img = cv2.convertScaleAbs(img_norm, alpha=(255.0 / np.amax(img_norm)))
                # img=img.transpose((1,2,0))
                # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                # img=cv2.resize(img, (256,256))
                # cv2.imshow("image_part", img)
                # cv2.waitKey()


                # = sample vae.decode(img)
                x_samples_ddim = model.decode_first_stage(samples_ddim)   # decoder

                image = torch.clamp((batch["image"]+1.0)/2.0, min=0.0, max=1.0)   # 원본 이미지
                mask = torch.clamp((batch["mask"]+1.0)/2.0, min=0.0, max=1.0)     # binary인 mask
                masked = torch.clamp((batch["masked_image"] + 1.0) / 2.0, min=0.0, max=1.0)  # binary인 mask
                predicted_image = torch.clamp((x_samples_ddim+1.0)/2.0,   min=0.0, max=1.0)   # masking 영역에 넣기 위해 임의로 만들어낸 이미지

                # check_img=(predicted_image.cpu().numpy()[0])
                # img_norm = cv2.normalize(check_img, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
                # img = cv2.convertScaleAbs(img_norm, alpha=(255.0 / np.amax(img_norm)))
                # img=img.transpose((1,2,0))
                # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                # cv2.imshow("image_part", img)
                # cv2.waitKey()

                #inpainted = (1-mask)*image+mask*predicted_image   # 원본이미지에 합성한 것
                #inpainted = inpainted.cpu().numpy().transpose(0,2,3,1)[0]*255
                # inpainted = predicted_image.cpu().numpy().transpose(0, 2, 3, 1)[0] * 255
                # Image.fromarray(inpainted.astype(np.uint8)).save(outpath)

                outpath=outpath.split(".png")[0]
                ori_outpath = outpath + ".png"
                mask_outpath = outpath + "_mask.png"
                masked_outpath = outpath + "_masked.png"
                inpainted_outpath = outpath + "_inpainted.png"


                ori_img = image.cpu().numpy().transpose(0, 2, 3, 1)[0] * 255
                mask_img = mask.cpu().numpy().transpose(0, 2, 3, 1)[0] * 255
                masked_img = masked.cpu().numpy().transpose(0, 2, 3, 1)[0] * 255
                inpainted_img = predicted_image.cpu().numpy().transpose(0, 2, 3, 1)[0] * 255

                Image.fromarray(ori_img.astype(np.uint8)).save(ori_outpath)
                mask_img= np.squeeze(mask_img, axis=2)
                Image.fromarray(mask_img.astype(np.uint8)).save(mask_outpath)
                Image.fromarray(masked_img.astype(np.uint8)).save(masked_outpath)
                Image.fromarray(inpainted_img.astype(np.uint8)).save(inpainted_outpath)


                #batch["image"].cpu().numpy()
                # check_img=(batch["image"].cpu().numpy()[0])
                # img_norm = cv2.normalize(check_img, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
                # img = cv2.convertScaleAbs(img_norm, alpha=(255.0 / np.amax(img_norm)))
                # img=img.transpose((1,2,0))
                # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                # cv2.imshow("image_part", img)